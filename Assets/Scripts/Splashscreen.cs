﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splashscreen : MonoBehaviour
{
    public GameObject splashscreen;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SplashDestroy());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator SplashDestroy()
    {
        yield return new WaitForSeconds(4);
        Destroy(splashscreen);
    }
}