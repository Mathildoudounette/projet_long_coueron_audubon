﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraClamp : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = new Vector3(
            Mathf.Clamp(transform.localPosition.x, -15f, 15f),
            Mathf.Clamp(transform.localPosition.y, -5f, 10f),
            transform.localPosition.z);
    }
}
