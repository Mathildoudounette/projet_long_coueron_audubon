﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class POI_Button : MonoBehaviour
{
    public CameraTransition c;
    
    public Transform newCameraPosition;
    public Transform lookTarget;

    public Transform originalLook;
    public Transform originalPosition;

    public GameObject dossierPrint;

    public GameObject assetsSpePOI;

    public static bool poiOpen = false;

    void Start()
    {
        dossierPrint.SetActive(false);
        assetsSpePOI.SetActive(false);
    }


    public void TaskOnClick()
    {
        c.cameraTarget = originalPosition;
    }


    public void OnMouseDown()
    {
		if (poiOpen == false)
		{
            Debug.Log("click");
            c.cameraTarget = newCameraPosition;
            c.lookTarget = lookTarget;
            c.isInTransition = true;
            dossierPrint.SetActive(true);
            assetsSpePOI.SetActive(true);
            poiOpen = true;
        }

    }

}
