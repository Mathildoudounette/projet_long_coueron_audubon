﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class POI_buttonBackup : MonoBehaviour
{
    public CameraTransition c;

    public Transform newCameraPosition;
    public Transform lookTarget;
    public GameObject dossierPrint;
    public GameObject assetSpePOI;



    public void OnMouseDown()
    {
        Debug.Log("click");
        c.cameraTarget = newCameraPosition;
        c.lookTarget = lookTarget;
       
        c.isInTransition = true;
        dossierPrint.SetActive(false);

        assetSpePOI.SetActive(false);

        POI_Button.poiOpen = false;

    }



}


