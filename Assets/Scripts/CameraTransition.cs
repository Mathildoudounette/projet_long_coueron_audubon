﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransition : MonoBehaviour
{
    public Transform cameraTarget;
    public Transform lookTarget;
    public float speed = 1;
    public float rotationSpeed = 1;
    public bool isInTransition;

    public Transform originalPosition;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isInTransition)
        {
            transform.parent.position = Vector3.Lerp(transform.parent.position, cameraTarget.position, speed * Time.deltaTime);


            Quaternion targetRotation = Quaternion.LookRotation(lookTarget.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
            Debug.Log((transform.parent.position - cameraTarget.position).magnitude);
            if ((transform.parent.position - cameraTarget.position).magnitude < 0.5f)
            {
                isInTransition = false;
            }
        }

		/*if (cameraTarget == true)
		{
            cameraTarget = lookTarget.SetActive(false);
        }*/
        
    }
}
