﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetsLauncher : MonoBehaviour
{
    public GameObject Assets;
    public AudioSource mainSound;

    // Start is called before the first frame update
    void Start()
    {
        Assets.SetActive(false);
        StartCoroutine("ActiveAssets");

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator ActiveAssets()
    {
        yield return new WaitForSeconds(12);
        Assets.SetActive(true);
        mainSound.Play();
    }

}
